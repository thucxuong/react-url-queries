This is a helper component that helps working with query params easier.

# Requirement

You need to have `react-router` setup in your application before using this package.

```
import React from 'react';
import { render } from 'react-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Home from 'components/Home';

const store = createStore(); // create redux store
const history = createHistory(); // history

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route exact path='/home' component={Home} />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app'),
);

```

This is a typical setup for `react-redux` app. If you've already have this setup. You can use this package without problem.


# Installation

```
npm install --save react-url-queries
```

OR

```
yarn add react-url-queries
```

## Props

**allowedParams (Array of string)**: of string sepcified all the params that can be passed to child component

## Passing Props (Children received props)

**params (Object)**: Filtered query params object which contains only allowed params

**updateQueries (Function)**: A function for updating query params to URL after manipulation, disallowed params will be filtered out before update to URL

**search (String)**: A complete search string in the URL pass to children

## Usage

```
import URLQueries from 'react-url-queries';

<URLQueries allowedParams={['name', 'price']}>
  <FilterComponent />
</URLQueries>

<URLQueries allowedParams={['name', 'price']}>{({params, updateQueries, search}) => {
 // read the params object and re-populate to the filter component
 // Do something here with params Object
 // Update the queries by calling updateQueries(new_params_obj)
}}
</URLQueries>
```


