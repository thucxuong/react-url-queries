import React from 'react';
import PropTypes from 'prop-types';
import qs from 'query-string';
import { withRouter } from 'react-router';

const filterAllowedParams = (queries, allowedParams) => {
  return allowedParams.reduce((acc, param) => {
    if (Object.prototype.hasOwnProperty.call(queries, param)) {
      acc[param] = queries[param];
    }
    return acc;
  }, {});
};

class URLQueries extends React.Component {
  constructor(props) {
    super(props);
    this.updateQueries = this.updateQueries.bind(this);
  }
  /**
   * update params object to address bar URL
   * @param {Object} queries Params object
   */
  updateQueries(queries) {
    const { location, history, allowedParams } = this.props;
    const allowedQueriesParams = filterAllowedParams(queries, allowedParams);
    const newQueryString = qs.stringify({ ...qs.parse(location.search), ...allowedQueriesParams });
    history.push(`${location.pathname}${newQueryString ? `?${newQueryString}` : ''}`);
  }
  render() {
    const { location, allowedParams, children } = this.props;
    const passProps = { params: {}, updateQueries: this.updateQueries, search: location.search };
    const params = qs.parse(location.search);
    if (!allowedParams || allowedParams.length === 0) {
      passProps.params = params;
    }
    passProps.params = filterAllowedParams(params, allowedParams);
    if (typeof children === 'function') {
      return children(passProps);
    }
    return React.cloneElement(children, passProps);
  }
}

URLQueries.propTypes = {
  children: PropTypes.any,
  allowedParams: PropTypes.array,
  history: PropTypes.object,
  location: PropTypes.object,
};

export default withRouter(URLQueries);

